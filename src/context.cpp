/* libmegadebrid - LGPL - Copyright 2019 - zadig */

#include <boost/property_tree/json_parser.hpp>
#include <boost/property_tree/ptree.hpp>

#include <megadebrid/config.hpp>
#include <megadebrid/context.hpp>
#include <megadebrid/net/request.hpp>
#include <megadebrid/utils/log.hpp>
#include <megadebrid/utils/url.hpp>

namespace megadebrid {

void Context::set_account(std::unique_ptr< Account > a) {
  _account = std::move(a);
}

Context::LoginCode Context::login(void) {
  auto sock = this->net_context().new_stream();
  if (!sock || !sock->connect()) {
    return LoginCode::LOGIN_UNKNOWN_ERROR;
  }
  std::string uri(config::api::uri::login);
  uri += "&login=";
  auto enc =
      utils::url_encode(this->curl_session(), this->account().username());
  uri += enc;
  uri += "&password=";
  enc = utils::url_encode(this->curl_session(), this->account().password());
  uri += enc;
  LOG(Trace, "Login final url = ", uri);

  net::Request r(std::move(sock),
                 config::endpoint_hostname,
                 net::Request::GET,
                 uri.c_str());

  if (!r.execute()) {
    return LoginCode::LOGIN_UNKNOWN_ERROR;
  }

  boost::property_tree::ptree tree;
  try {
    std::stringstream ss;
    ss << r.response_body();
    boost::property_tree::read_json(ss, tree);
    auto response_code = tree.get< std::string >("response_code");
    if (!response_code.compare("ok")) {
      /* Login success */
      LOG(Info, "Login success");
      auto token = tree.get< std::string >("token");
      LOG(Trace, "Token: ", token);
      _account->set_token(token.c_str());

      auto ve = tree.get< std::time_t >("vip_end");
      LOG(Trace, "Vip end: ", ve);
      _account->set_vip_end(ve);
      return LoginCode::LOGIN_SUCCESS;
    } else {
      auto err = tree.get< std::string >("response_text");
      LOG(Error, "Unable to login: ", err);
      return LoginCode::LOGIN_INVALID_CREDENTIALS;
    }
  } catch (const boost::property_tree::json_parser_error& e) {
    /* Cannot parse JSON */
    LOG(Error, "Unable to parse JSON:", e.message());
    return LoginCode::LOGIN_UNKNOWN_ERROR;
  } catch (const boost::property_tree::ptree_bad_data& e) {
    /* Cannot access to a JSON child */
    LOG(Error, "Unable to access to JSON child: ", e.what());
    return LoginCode::LOGIN_UNKNOWN_ERROR;
  }
}

Context::DebridCode Context::debrid(const std::string& link) {
  std::string b("&link=");
  auto enc = utils::url_encode(this->curl_session(), link);
  b += enc;
  return this->debrid_with_body(b);
}

Context::DebridCode Context::debrid(const std::string& link,
                                    const std::string& password) {
  std::string b("&link=");
  auto enc = utils::url_encode(this->curl_session(), link);
  b += enc;
  b += "&password=";
  enc = utils::url_encode(this->curl_session(), password);
  b += enc;
  return this->debrid_with_body(b);
}

Context::DebridCode Context::debrid_with_body(const std::string& body) {
  if (!this->account().token()) {
    return DebridCode::DEBRID_NO_ACCOUNT;
  }
  if (this->account().vip_end() == 0) {
    return DebridCode::DEBRID_NO_VIP;
  }

  auto sock = this->net_context().new_stream();
  if (!sock || !sock->connect()) {
    return DebridCode::DEBRID_UNKNOWN_ERROR;
  }
  std::string uri(config::api::uri::debrid);
  uri += "&token=";
  std::string token(*this->account().token(),
                    *this->account().token() + config::token_len);
  auto enc = utils::url_encode(this->curl_session(), token);
  uri += enc;
  LOG(Trace, "Debrid final url = ", uri);

  net::Request r(std::move(sock),
                 config::endpoint_hostname,
                 net::Request::POST,
                 uri.c_str());

  r.set_body(body);

  if (!r.execute()) {
    return DebridCode::DEBRID_UNKNOWN_ERROR;
  }

  boost::property_tree::ptree tree;
  try {
    std::stringstream ss;
    ss << r.response_body();
    boost::property_tree::read_json(ss, tree);
    auto response_code = tree.get< std::string >("response_code");
    if (!response_code.compare("ok")) {
      /* Debrid success */
      LOG(Info, "Debrid success");
      auto debrid_link = tree.get< std::string >("debridLink");
      LOG(Info, "Debrid link:", debrid_link);
      return DebridCode::DEBRID_SUCCESS;
    } else {
      auto err = tree.get< std::string >("response_text");
      LOG(Error, "Unable to debrid: ", err);
      return DebridCode::DEBRID_UNKNOWN_ERROR;
      ;
    }
  } catch (const boost::property_tree::json_parser_error& e) {
    /* Cannot parse JSON */
    LOG(Error, "Unable to parse JSON:", e.message());
    return DebridCode::DEBRID_UNKNOWN_ERROR;
  } catch (const boost::property_tree::ptree_bad_data& e) {
    /* Cannot access to a JSON child */
    LOG(Error, "Unable to access to JSON child: ", e.what());
    return DebridCode::DEBRID_UNKNOWN_ERROR;
  }
}

Context::~Context(void) {
  curl_easy_cleanup(_curl_session);
}

} // end namespace megadebrid
