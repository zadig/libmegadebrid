/* libmegadebrid - LGPL - Copyright 2019 - zadig */

#include <functional>
#include <iostream>

#include <megadebrid/utils/log.hpp>

namespace megadebrid::utils {

#define RESET "\033[0m"
#define BOLDRED "\033[1m\033[31m"
#define BOLDYELLOW "\033[1m\033[33m"
#define BOLDBLUE "\033[1m\033[34m"
#define BOLDCYAN "\033[1m\033[36m"
#define BOLDWHITE "\033[1m\033[37m"

static const char* LogLevelFmt[] = {
    BOLDRED "[error]" RESET,
    BOLDYELLOW "[warn ]" RESET,
    BOLDBLUE "[info ]" RESET,
    BOLDCYAN "[debug]" RESET,
    BOLDWHITE "[trace]" RESET,
};

static LogLevel current_logging_level = LogLevel::Error;

void set_logging_level(LogLevel lvl) {
  current_logging_level = lvl;
}

LogLevel get_logging_level(void) {
  return current_logging_level;
}

const char* get_logging_level_fmt(LogLevel lvl) {
  return LogLevelFmt[lvl];
}

} // end namespace megadebrid::utils
