/* libmegadebrid - LGPL - Copyright 2019 - zadig */

#include <megadebrid/utils/url.hpp>

namespace megadebrid::utils {

std::string url_encode(CURL* curl, const std::string& str) {
  auto res =
      curl_easy_escape(curl, str.c_str(), static_cast< int >(str.size()));
  auto ret = std::string(res);
  curl_free(res);

  return ret;
}

} // namespace megadebrid::utils
