/* libmegadebrid - LGPL - Copyright 2019 - zadig */

#include <algorithm>

#include <megadebrid/account.hpp>
#include <megadebrid/utils/log.hpp>

namespace megadebrid {

const std::string& Account::username(void) const {
  return _username;
}

const std::string& Account::password(void) const {
  return _password;
}

optional< const char* const > Account::token(void) const {
  if (_token[0]) {
    return _token.data();
  } else {
    return nullopt;
  }
}

std::time_t Account::vip_end(void) const {
  return _vip_end;
}

void Account::set_vip_end(std::time_t ve) {
  _vip_end = ve;
}

void Account::set_token(const char* const token) {
  std::copy_n(token, config::token_len, _token.data());
}

} // end namespace megadebrid
