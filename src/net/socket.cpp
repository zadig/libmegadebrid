/* libmegadebrid - LGPL - Copyright 2019 - zadig */

#include <boost/asio/connect.hpp>
#include <boost/asio/ssl.hpp>

#include <megadebrid/net/socket.hpp>
#include <megadebrid/utils/log.hpp>

namespace megadebrid::net {

bool Socket::connect(void) {
  boost::system::error_code bcode;

  /* TCP connect */
  boost::asio::connect(_socket.next_layer(),
                       _resolver_results.begin(),
                       _resolver_results.end(),
                       bcode);
  if (bcode) {
    LOG(Error, "Unable to connect: ", bcode.message());
    return false;
  }

  LOG(Trace, "Socket connected");

  /* TLS handshake */
  _socket.handshake(boost::asio::ssl::stream_base::client, bcode);
  if (bcode) {
    LOG(Error, "Unable to TLS handshake: ", bcode.message());
    return false;
  }
  LOG(Trace, "TLS handshake done");

  return true;
}

void Socket::close(void) {
  boost::system::error_code bcode;
  _socket.shutdown(bcode);
}

Socket::~Socket(void) {
  this->close();
}

} // end namespace megadebrid::net
