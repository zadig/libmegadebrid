/* libmegadebrid - LGPL - Copyright 2019 - zadig */

#include <boost/beast/core.hpp>

#include <megadebrid/net/request.hpp>
#include <megadebrid/utils/log.hpp>

#ifndef LIBVERSION
#define LIBVERSION "unknown"
#endif

namespace megadebrid::net {

static boost::beast::http::verb get_beast_method(Request::Methods m) {
  switch (m) {
    case Request::Methods::POST:
      return boost::beast::http::verb::post;
    case Request::Methods::PUT:
      return boost::beast::http::verb::put;
    case Request::Methods::GET:
    default:
      return boost::beast::http::verb::get;
  }
}

Request::Request(std::unique_ptr< Socket > socket,
                 const char* const host,
                 Request::Methods method,
                 const char* const path)
    : _socket(std::move(socket)), _request{get_beast_method(method), path, 11} {
  _request.set(boost::beast::http::field::user_agent,
               "libmegadebrid/" LIBVERSION);
  _request.set(boost::beast::http::field::host, host);
  if (method == Request::POST) {
    _request.set(boost::beast::http::field::content_type,
                 "application/x-www-form-urlencoded");
  }
}

void Request::set_body(const std::string& b) {
  _request.body() = b.c_str();
  _request.prepare_payload();
}

bool Request::execute(void) {
  boost::system::error_code bcode;

  boost::beast::http::write(_socket->boost_socket(), _request, bcode);
  LOG(Trace, "boost error code = ", bcode);

  if (!bcode) {
    boost::beast::flat_buffer buffer;
    _response = std::make_unique<
        boost::beast::http::response< boost::beast::http::string_body > >();
    boost::beast::http::read(_socket->boost_socket(),
                             buffer,
                             *_response,
                             bcode);
    LOG(Trace, "boost error code = ", bcode);
    if (!bcode) {
      LOG(Debug, "Request success. Body: ", _response->body());
      return true;
    } else {
      LOG(Error, "Unable to read answer from endpoint: ", bcode.message());
      _response = nullptr;
      return false;
    }
  } else {
    LOG(Error, "Unable to send request to the endpoint: ", bcode.message());
    return false;
  }
}

} // end namespace megadebrid::net
