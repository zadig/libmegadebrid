/* libmegadebrid - LGPL - Copyright 2019 - zadig */

#include <megadebrid/config.hpp>
#include <megadebrid/net/context.hpp>
#include <megadebrid/utils/log.hpp>

// Compatibility with OpenSSL 1.1.1
#if (OPENSSL_VERSION_NUMBER >= 0x10101000L)
const SSL_METHOD* SSLv23_client_method(void) {
  return nullptr;
}

const SSL_METHOD* SSLv23_method(void) {
  return nullptr;
}

const SSL_METHOD* SSLv23_server_method(void) {
  return nullptr;
}
#endif

namespace megadebrid::net {

std::unique_ptr< Socket > Context::new_stream(void) {
  if (!_resolver_results && !this->resolve_endpoint()) {
    LOG(Warn, "Unable to resolve hostname");
    return nullptr;
  } else {
    LOG(Trace, "New socket created");
    return std::make_unique< Socket >(this->io_context(),
                                      this->tls_context(),
                                      this->resolver_results());
  }
}

bool Context::resolve_endpoint(void) {
  boost::system::error_code bcode;
  const auto results = _resolver.resolve(config::endpoint_hostname,
                                         config::endpoint_port_str,
                                         bcode);
  LOG(Trace, "boost error code = ", bcode);
  if (!bcode) {
    _resolver_results = std::make_unique<
        const boost::asio::ip::basic_resolver_results< boost::asio::ip::tcp > >(
        std::move(results));
    return true;
  } else {
    LOG(Error, "Unable to resolve endpoint: ", bcode.message());
    return false;
  }
}

Context::Context(void)
    : _io_context(),
      _tls_context{boost::asio::ssl::context::tlsv12_client},
      _resolver{_io_context},
      _resolver_results(nullptr) {
  this->resolve_endpoint();
}

} // end namespace megadebrid::net
