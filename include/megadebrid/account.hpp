/* libmegadebrid - LGPL - Copyright 2019 - zadig */

#pragma once

#include <chrono>
#include <functional>
#include <memory>
#include <string>

#include <megadebrid/config.hpp>
#include <megadebrid/optional.hpp>

namespace megadebrid {

/// \brief An account
class Account {
private:
  /// \brief Username of the account
  std::string _username;

  /// \brief Password of the account
  std::string _password;

  /// \brief Token
  std::array< char, config::token_len > _token;

  /// \brief VIP end
  std::time_t _vip_end;

public:
  /// \brief Get the username
  ///
  /// \return Username
  const std::string& username(void) const;

  /// \brief Set the username
  ///
  /// \param args Username to set
  template < typename... Ts >
  void set_username(Ts... args) {
    _username = std::string(args...);
  }

  /// \brief Get the password
  ///
  /// \return The password
  const std::string& password(void) const;

  /// \brief Set the password
  ///
  /// \param args Password to set
  template < typename... Ts >
  void set_password(Ts... args) {
    _password = std::string(args...);
  }

  /// \brief Get the token
  ///
  /// \return The token
  optional< const char* const > token(void) const;

  /// \brief Set the token
  ///
  /// \param token The token
  void set_token(const char* const token);

  /// \brief Get the vip_end
  ///
  /// \return the vip_end
  std::time_t vip_end(void) const;

  /// \brief Set the vip_end
  ///
  /// \param ve the vip_end
  void set_vip_end(std::time_t ve);

public:
  /// \brief Construct an account
  ///
  /// \param username Username
  /// \param password Password
  Account(const std::string& username, const std::string& password)
      : _username(username), _password(password), _token{}, _vip_end(0) {}
};

} // end namespace megadebrid
