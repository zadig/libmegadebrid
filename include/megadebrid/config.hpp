/* libmegadebrid - LGPL - Copyright 2019 - zadig */

#pragma once

#include <cstddef>
#include <cstdint>

namespace megadebrid::config {

/// \brief Length of the token
const std::size_t token_len = 32;

/// \brief Endpoint hostname
const char* const endpoint_hostname = "www.mega-debrid.eu";

/// \brief Endpoint port, string format
const char* const endpoint_port_str = "443";

/// \brief Endpoint port, integer format
const uint16_t endpoint_port_int = 443;

/// \brief API's URIs
namespace api::uri {

/// \brief Login uri
const char* const login =
    "https://www.mega-debrid.eu/api.php?action=connectUser";

/// \brief Debrid uri
const char* const debrid = "https://www.mega-debrid.eu/api.php?action=getLink";

/// \brief History uri
const char* const history =
    "https://www.mega-debrid.eu/api.php?action=getUserHistory";

} // namespace api::uri

} // end namespace megadebrid::config
