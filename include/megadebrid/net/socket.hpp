/* libmegadebrid - LGPL - Copyright 2019 - zadig */

#pragma once

#include <boost/asio/ip/tcp.hpp>
#include <boost/asio/ssl.hpp>

namespace megadebrid::net {

/// \brief Basic wrapper for sockets
class Socket {
private:
  /// \brief asio socket
  boost::asio::ssl::stream< boost::asio::ip::tcp::socket > _socket;

  /// \brief asio resolver results
  const boost::asio::ip::basic_resolver_results< boost::asio::ip::tcp >&
      _resolver_results;

public:
  /// \brief Constructor
  ///
  /// \param io_ctx asio IO context
  /// \param tls_ctx asio TLS context
  Socket(boost::asio::io_context& io_ctx,
         boost::asio::ssl::context& tls_ctx,
         const boost::asio::ip::basic_resolver_results< boost::asio::ip::tcp >&
             resolver_results)
      : _socket{io_ctx, tls_ctx}, _resolver_results(resolver_results) {}

  /// \brief Copy constructor
  Socket(const Socket&) = delete;

public:
  /// \brief Connect
  ///
  /// \return true if connected, else false
  bool connect(void);

  /// \brief Get boost socket
  ///
  /// \return boost socket
  boost::asio::ssl::stream< boost::asio::ip::tcp::socket >& boost_socket(void) {
    return _socket;
  }

  /// \brief Close the socket
  void close(void);

public:
  /// \brief Destructor
  ~Socket(void);
};

} // end namespace megadebrid::net
