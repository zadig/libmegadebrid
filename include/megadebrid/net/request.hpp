/* libmegadebrid - LGPL - Copyright 2019 - zadig */

#pragma once

#include <memory>

#include <boost/beast/http.hpp>

#include <megadebrid/net/socket.hpp>

namespace megadebrid::net {

/// \brief a HTTP request
class Request {
private:
  /// \brief The related socket
  std::unique_ptr< Socket > _socket;

  /// \brief HTTP beast request
  boost::beast::http::request< boost::beast::http::string_body > _request;

  /// \brief HTTP beast response
  std::unique_ptr<
      boost::beast::http::response< boost::beast::http::string_body > >
      _response;

public:
  /// \brief Enumeration of HTTP request methods
  enum Methods {
    GET,  ///< GET method
    POST, ///< POST mehod
    PUT   ///< PUT method
  };

  /// \brief Constructor
  ///
  /// \param socket The socket to use
  /// \param host The host
  /// \param method The method (GET, POST ...)
  /// \param path The path
  Request(std::unique_ptr< Socket > socket,
          const char* const host,
          Request::Methods method,
          const char* const path);

  /// \brief Execute the request
  ///
  /// \return true if everything is okay, else false
  bool execute(void);

  /// \brief Set the request body
  ///
  /// \param b The body
  void set_body(const std::string& b);

  /// \brief Access to body
  ///
  /// \return Body of the response
  const std::string& response_body(void) { return _response->body(); }

  /// \brief Copy constructor
  Request(const Request&) = delete;
};

} // end namespace megadebrid::net
