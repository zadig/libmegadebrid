/* libmegadebrid - LGPL - Copyright 2019 - zadig */

#pragma once

#include <boost/asio/ip/resolver_service.hpp>
#include <boost/asio/ip/tcp.hpp>
#include <boost/asio/ssl.hpp>

#include <megadebrid/net/socket.hpp>

// Compatibility with OpenSSL 1.1.1
#if (OPENSSL_VERSION_NUMBER >= 0x10101000L)
const SSL_METHOD* SSLv23_client_method(void);

const SSL_METHOD* SSLv23_method(void);

const SSL_METHOD* SSLv23_server_method(void);
#endif

namespace megadebrid {
/// Forward declaration
class Context;
using MegadebridCtx = Context;
} // end namespace megadebrid

namespace megadebrid::net {

class Context {
  friend class MegadebridCtx;

private:
  /// \brief asio IO context
  boost::asio::io_context _io_context;

  /// \brief asio TLS context
  boost::asio::ssl::context _tls_context;

  /// \brief asio resolver
  boost::asio::ip::tcp::resolver _resolver;

  std::unique_ptr<
      const boost::asio::ip::basic_resolver_results< boost::asio::ip::tcp > >
      _resolver_results;

private:
  /// \brief Get asio IO context
  ///
  /// \return asio IO context
  boost::asio::io_context& io_context(void) { return _io_context; }

  /// \brief Get TLS context
  ///
  /// \return asio TLS context
  boost::asio::ssl::context& tls_context(void) { return _tls_context; }

  /// \brief Get resolver results
  ///
  /// \return Resolver results
  const boost::asio::ip::basic_resolver_results< boost::asio::ip::tcp >&
  resolver_results(void) const {
    return *_resolver_results;
  }

public:
  /// \brief Get a new socket
  ///
  /// \return new socket
  std::unique_ptr< Socket > new_stream(void);

public:
  /// \brief Resolve the endpoint hostname
  ///
  /// \return true if success, else false
  bool resolve_endpoint(void);

public:
  /// \brief Constructor
  Context(void);

  /// \brief Copy constructor
  Context(const Context&) = delete;
};

} // end namespace megadebrid::net
