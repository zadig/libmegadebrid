/* libmegadebrid - LGPL - Copyright 2019 - zadig */

#include <iostream>

#define FILENAME \
  (strrchr(__FILE__, '/') ? strrchr(__FILE__, '/') + 1 : __FILE__)

#ifndef __FUNCTION__ // 1
#ifndef __func__     // 2
#define FUNCTION_NAME ""
#else
#define FUNCTION_NAME ":"__func__
#endif // 2
#else
#define FUNCTION_NAME ":"__FUNCTION__
#endif // 1

namespace megadebrid::utils {

/// \brief Logging level
/// This enum describe the well-known logging level
enum LogLevel {
  Error = 0x00, ///< Error level
  Warn = 0x01,  ///< Warning level
  Info = 0x02,  ///< Info level
  Debug = 0x03, ///< Debug level
  Trace = 0x04  ///< Trace level
};

/// \brief Set the logging level for the current execution
///
/// \param lvl The wanted logging level
void set_logging_level(LogLevel lvl);

/// \brief Get the logging level for the current execution
///
/// \return The current logging level
LogLevel get_logging_level(void);

/// \brief Get the prefix for a logging message, depending on the level
///
/// \param lvl Wanted logging level message
///
/// \return The logging message prefix
const char* get_logging_level_fmt(LogLevel lvl);

/// \brief Unwrap a variadic log (last arg)
///
/// \param t Last type to unwrap
template < typename T, typename... Types >
typename std::enable_if< sizeof...(Types) == 0, void >::type log_unwrap(
    T t, Types...) {
  std::cout << t;
}

/// \brief Unwrap a variadic log
///
/// \param t First type
/// \param ts Other types
template < typename T, typename... Types >
typename std::enable_if< sizeof...(Types) != 0, void >::type log_unwrap(
    T t, Types... ts) {
  std::cout << t;
  log_unwrap(ts...);
}

/// \brief Logging function
/// This function log things
///
/// \param lvl Logging level
/// \param ts Things to log
template < typename... Types >
void log(LogLevel lvl, Types... ts) {
  if (lvl <= get_logging_level()) {
    std::cout << get_logging_level_fmt(lvl) << ' ';
    log_unwrap(ts...);
    std::cout << std::endl;
  }
}

#define LOG(_lvl_, ...)                     \
  log(::megadebrid::utils::LogLevel::_lvl_, \
      '[',                                  \
      FILENAME,                             \
      FUNCTION_NAME,                        \
      ':',                                  \
      __LINE__,                             \
      "] ",                                 \
      __VA_ARGS__)

} // end namespace megadebrid::utils
