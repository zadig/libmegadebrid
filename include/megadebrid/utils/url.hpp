/* libmegadebrid - LGPL - Copyright 2019 - zadig */

#include <string>

#include <curl/curl.h>

namespace megadebrid::utils {

std::string url_encode(CURL* curl, const std::string& str);

} // namespace megadebrid::utils
