/* libmegadebrid - LGPL - Copyright 2019 - zadig */

#pragma once

#include <memory>

#include <boost/asio/ip/tcp.hpp>
#include <boost/asio/ssl.hpp>

#include <curl/curl.h>

#include <megadebrid/account.hpp>
#include <megadebrid/net/context.hpp>

namespace megadebrid {

/// \brief MegaDebrid global context
class Context {
private:
  /// \brief Network context
  net::Context _net_context;

  /// \brief related account
  std::unique_ptr< Account > _account;

  /// \brief Curl session, for encoding/decoding
  CURL* _curl_session;

public:
  /// \brief Get the related account
  ///
  /// \return The related account
  const Account& account(void) const { return *_account; }

  /// \brief Set the related account
  ///
  /// \param a The related account
  void set_account(std::unique_ptr< Account > a);

  /// \brief Get the network context
  ///
  /// \return The network context
  net::Context& net_context(void) { return _net_context; }

  /// \brief Get the curl session
  ///
  /// \return The curl session
  CURL* curl_session(void) { return _curl_session; }

public:
  /// \brief Constructor
  Context(void)
      : _net_context(), _account(nullptr), _curl_session(curl_easy_init()) {}

  /// \brief Copy constructor
  Context(const Context&) = delete;

  /// \brief Destructor
  ~Context(void);

public:
  enum LoginCode {
    LOGIN_SUCCESS = 0,
    LOGIN_INVALID_CREDENTIALS = 1,
    LOGIN_BANNED = 2,
    LOGIN_UNKNOWN_ERROR = 3
  };

  /// \brief Log in to the account
  ///
  /// \return Login code enum
  LoginCode login(void);

public:
  enum DebridCode {
    DEBRID_SUCCESS = 0,
    DEBRID_BAD_LINK = 1,
    DEBRID_NO_ACCOUNT = 2,
    DEBRID_NO_VIP = 3,
    DEBRID_UNKNOWN_ERROR = 4
  };

private:
  /// \brief Debrid a link with POST body
  ///
  /// \param b body
  ///
  /// \return Debrid code
  DebridCode debrid_with_body(const std::string& b);

public:
  /// \brief Debrid a link
  ///
  /// \param link The link to debrid
  ///
  /// \return Debrid code enum
  DebridCode debrid(const std::string& link);

  /// \brief Debrid a link with a password
  ///
  /// \param link The link to debrid
  /// \param password the password of the link
  ///
  /// \return Debrid code enum
  DebridCode debrid(const std::string& link, const std::string& password);
};

} // end namespace megadebrid
