/* libmegadebrid - LGPL - Copyright 2019 - zadig */

#pragma once

#if __has_include(<optional>)
#include <optional>

namespace megadebrid {

template < class T >
using optional = std::optional< T >;
const auto nullopt = std::nullopt;

} // end namespace megadebrid

#elif __has_include(<experimental/optional>)
#include <experimental/optional>

namespace megadebrid {

template < class T >
using optional = std::experimental::optional< T >;
const auto nullopt = std::experimental::nullopt;

} // end namespace megadebrid

#elif __has_include(<boost/optional.hpp>)
#include <boost/optional.hpp>

namespace megadebrid {

template < class T >
using optional = boost::optional< T >;
const auto nullopt = boost::none;

} // end namespace megadebrid

#else
#error "Couldn't find a suitable version for optional"
#endif
