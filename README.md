# libmegadebrid

C++ library for querying [Mega-Debrid API](https://www.mega-debrid.eu/index.php?page=api)

## Description

`libmegadebrid` is a C++ library for Mega-Debrid. You can use inside your own C++ project for unrestricking links, or for creating your own mega-debrid client tool.

## Installation

### Prerequisites

`libmegadebrid` requires the following prerequisites:

  * C++17
  * [Boost](https://www.boost.org/) (with _system_)
  * [OpenSSL](https://www.openssl.org/) (required by Boost)
  * [libcurl](https://curl.haxx.se/libcurl/) (required for encoding/decoding)
  * [CMake](https://cmake.org/) (for building the library)
  * [Doxygen](http://www.doxygen.nl/) (for compiling the documentation)

### Prerequisites - Debian

TODO

### Prerequisites - ArchLinux

TODO

### Compiling

```
mkdir build && cd build
cmake .. # CXX=clang++ CC=clang cmake ..
make
sudo make install
```

## Documentation

The code is well documented. You can build the documentation using Doxygen:
```
doxygen Doxyfile
```

## Example

```cpp
#include <megadebrid/context.hpp>

int main(const int argc, const char **argv, const char **envp) {
  (void) argc;
  (void) argv;
  (void) envp;

  megadebrid::utils::set_logging_level(megadebrid::utils::LogLevel::Trace);
  megadebrid::Context md_ctx;

  std::unique_ptr<megadebrid::Account> md_account = std::make_unique<megadebrid::Account>("MyAwesomeUser", "MyAwesomePassword");

  md_ctx.set_account(std::move(md_account));

  if (md_ctx.login() == megadebrid::Context::LOGIN_SUCCESS) {
    md_ctx.debrid(argv[1]);
  }
}

```

## Authors

zadig `<zadig@riseup.net>`
